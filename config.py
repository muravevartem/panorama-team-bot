import os


class Config:
    TELEGRAM_TOKEN = os.getenv('TELEGRAM_TOKEN')
    DATE_FORMAT = '%Y-%m-%d'


config = Config()
