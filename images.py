class Images:
    ESTIMATION = './static/estimation.png'
    QUITES_1 = './static/quotes_1.png'
    MAGIC = './static/magic.gif'
    OOPS = './static/oops.gif'
    MEGA_HEAL = './static/mega_heal.gif'
    HELLO_IT = './static/hello_it.gif'
    HELLO_GOPSTOP = './static/gopstop.gif'
    THIS_IS_FINE = './static/this_is_fine.gif'
    BELKA = './static/belka.gif'
    PRESS_F = './static/press_f.png'
    PRESS_F_CAT = './static/press_f_cat.png'
    SALARY = './static/salary_meme.jpg'



images = Images()
