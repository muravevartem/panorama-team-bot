from random import choice

from telebot import TeleBot
from telebot.types import BotCommand

from config import config
from images import images

bot = TeleBot(config.TELEGRAM_TOKEN)
bot.parse_mode = 'HTML'


def init_commands():
    bot.set_my_commands(commands=[
        BotCommand('/magic', 'Колдовство'),
        BotCommand('/help', 'Помощь'),
        BotCommand('/salary', 'Ветка'),
        BotCommand('/hello_from_team', 'Поприветствуем нового члена команды'),
        BotCommand('/oops', 'Что-то пошло не так'),
        BotCommand('/this_is_fine', 'This is fine'),
        BotCommand('/mega_heal', 'Mega heal'),
        BotCommand('/press_f', 'Press F')
    ])


@bot.message_handler(commands=['salary'])
def handle_salary(message):
    sticker = open(images.SALARY, 'rb')
    bot.send_message(chat_id=message.chat.id, text='Ветка пришла🌿')
    bot.send_sticker(chat_id=message.chat.id, sticker=sticker)


def randomHelloGif() -> str:
    return choice([images.HELLO_IT, images.HELLO_GOPSTOP])


@bot.message_handler(content_types=["new_chat_members"])
def handle_new_chat_members(message):
    usernames = list(map(lambda e: __tag_user(e), message.new_chat_members))
    welcome_gif = open(randomHelloGif(), 'rb')
    welcome_message = f'Добро пожаловать в команду, {", ".join(usernames)}!👋'

    bot.send_message(chat_id=message.chat.id, text=welcome_message, reply_to_message_id=message.id)
    bot.send_animation(chat_id=message.chat.id, animation=welcome_gif)


@bot.message_handler(commands=['hello_from_team'])
def handle_hello(message):
    message_text = message.text
    message_entities = message.entities
    usernames = list(map(lambda e: message_text[e.offset:e.offset + e.length],
                         filter(lambda e: e.type == 'mention', message_entities)))
    welcome_gif = open(randomHelloGif(), 'rb')
    if len(usernames) == 0:
        welcome_message = f'Добро пожаловать в команду!👋'
    elif len(usernames) == 1:
        welcome_message = f'Добро пожаловать в команду, {usernames[0]}!👋'
    else:
        welcome_message = 'Добро пожаловать всем в команду!👋'

    bot.send_animation(chat_id=message.chat.id, animation=welcome_gif)
    bot.send_message(chat_id=message.chat.id, text=welcome_message)


def randomPressF() -> str:
    return choice([images.PRESS_F_CAT, images.PRESS_F])


@bot.message_handler(content_types=['left_chat_member'])
def handle_left_chat_member(message):
    sticker = open(randomPressF(), 'rb')
    bot.send_sticker(chat_id=message.chat.id, sticker=sticker, reply_to_message_id=message.id)


@bot.message_handler(commands=['press_f'])
def handle_press_f_member(message):
    sticker = open(randomPressF(), 'rb')
    bot.send_sticker(chat_id=message.chat.id, sticker=sticker)


def randomThisIsFine() -> str:
    return choice([images.BELKA, images.THIS_IS_FINE])


@bot.message_handler(commands=['this_is_fine'])
def handle_quotes(message):
    gif_file = open(randomThisIsFine(), 'rb')
    bot.send_animation(chat_id=message.chat.id, reply_to_message_id=message.id, animation=gif_file)


@bot.message_handler(commands=['start'])
@bot.message_handler(commands=['help'])
def handle_start(message):
    help_text = ('Избавлю от порчи, сглаза и багов /magic\n'
                 'Представлю нового сотрудника команде /hello_from_team\n'
                 'Сообщу о ветке /salary\n'
                 'Объясню причину внештатной ситуации /oops')
    bot.send_message(chat_id=message.chat.id, text=help_text, reply_to_message_id=message.id)


@bot.message_handler(commands=['magic'])
@bot.message_handler(func=lambda message: 'вуншпунш' in message.text.lower() if message.text is not None else '')
def handle_magic(message):
    magic_gif = open(images.MAGIC, 'rb')
    bot.send_animation(chat_id=message.chat.id, reply_to_message_id=message.id, animation=magic_gif)


@bot.message_handler(commands=['oops'])
@bot.message_handler(func=lambda message: 'у нас сломался' in message.text.lower() if message.text is not None else '')
def handle_oops(message):
    gif_file = open(randomThisIsFine(), 'rb')
    bot.send_animation(chat_id=message.chat.id, reply_to_message_id=message.id, animation=gif_file)


@bot.message_handler(commands=['mega_heal'])
def handle_mega_heal(message):
    mega_heal_gif = open(images.MEGA_HEAL, 'rb')
    bot.send_animation(chat_id=message.chat.id, animation=mega_heal_gif)
    bot.send_message(chat_id=message.chat.id, text=f'MEGA HEAL!')

@bot.message_handler(func=lambda message: 'я заболел' in message.text.lower() if message.text is not None else '')
def handle_mega_heal(message):
    mega_heal_gif = open(images.MEGA_HEAL, 'rb')
    bot.send_animation(chat_id=message.chat.id, animation=mega_heal_gif)
    bot.send_message(chat_id=message.chat.id, text=f'{__tag_user(message.from_user)}, MEGA HEAL!', reply_to_message_id=message.id)

def __tag_user(user):
    if user.username is not None:
        return '@' + user.username
    return user.first_name


def infinity_polling():
    init_commands()
    bot.infinity_polling()
